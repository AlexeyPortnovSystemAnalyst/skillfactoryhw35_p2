import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.time.Duration;

import static org.junit.Assert.assertEquals;

public class StepDefinitions {


    private static final WebDriver webDriver = new ChromeDriver();
    static {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\89672\\OneDrive\\Документы\\projects\\cc-scenaro3\\chromedriver.exe");
       // webDriver = new ChromeDriver();
        //webDriver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        //webDriver.manage().window().maximize();
       // chooseCityPage = new ChooseCityPage(webDriver);
        //cityMenuPage = new CityMenuPage(webDriver);
    }

    @Given("url of h&m {string}")
    public void url_of_h_m(String url) {
    webDriver.get(url);
        // Write code here that turns the phrase above into concrete actions
//        throw new io.cucumber.java.PendingException();
    }

    @When("website is open start accept all cookie")
    public void website_is_open_start_accept_all_cookie() {
        webDriver.findElement(By.id("onetrust-reject-all-handler")).click();
    }

    @Then("start search {string}")
    public void start_search(String searchParam) {
       WebElement searchField = webDriver.findElement(By.id("main-search"));
        searchField.sendKeys(searchParam);
        searchField.submit();
    }

    @Then("click on first image")
    public void click_on_first_image() {
        webDriver.findElement(By.className("image-container")).click();
    }

    @Then("choose the size")
    public void choose_the_size() {
        webDriver.findElement(By.className("trigger-button")).click();
        webDriver.findElements(By.className("picker-option")).get(3).click(); //.click() option  active
    }

    @Then("click add button")
    public void click_add_button() {
        webDriver.findElement(By.className("button-buy")).click();
    }
}
